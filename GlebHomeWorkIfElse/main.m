//
//  main.m
//  GlebHomeWorkIfElse
//
//  Created by Chaban Nikolay on 10/13/15.
//  Copyright (c) 2015 GlebCherkashyn. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        float potSize = 10;
        float equityVsRange = 0.33;
        float oppBetSize = 8;
        float raiseSize = 20;
        float oppFoldVsRaiseStat = 0.66;
        float eqRaiseLimit = 0.65;
        float foldVsRaiseFact = (potSize+oppBetSize)/(potSize+oppBetSize+raiseSize);
        float potOdds = oppBetSize/(potSize+oppBetSize);
        if ((equityVsRange > potOdds)&&(equityVsRange<eqRaiseLimit)) {
            NSLog(@"Pot odds are %f. Your hand is good enought to call", potOdds);
        } else if (equityVsRange>eqRaiseLimit) {
            NSLog(@"Pot odds are %f. Your hand is to strong. You should raise", potOdds);
        } else if ((equityVsRange<potOdds)&&(oppFoldVsRaiseStat>=foldVsRaiseFact))  {
            NSLog(@"Pot odds are %f. Your hand is weak. You should bluff raise", potOdds);
        } else
        NSLog(@"Not this time body... You should fold");
    }
    return 0;
}
